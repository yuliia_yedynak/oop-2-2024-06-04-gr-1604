from abc import ABC, abstractmethod


class Employee(ABC):
    def __init__(self, id_, name):
        self.id = id_
        self.name = name

    @abstractmethod
    def calculate_payroll(self):
        pass
