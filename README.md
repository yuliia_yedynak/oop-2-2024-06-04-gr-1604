# oop_2_2023_10_31_gr1209

---

[video 1](https://youtu.be/vholI8CkEM8) - oop 2, inheritance

[video 2](https://youtu.be/Ki94gkFcFu4) oop 3, composition, methods: instance, class, static

[video 3](https://youtu.be/JpDbN5ct_h8) oop 4, exceptions, managed attributes, property
